package com.example.assignment7;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageView location = (ImageView) findViewById(R.id.btn_location);
        DatePicker datePicker = (DatePicker) findViewById(R.id.datePicker);

        String day = "Day = " + datePicker.getDayOfMonth();
        String month = "Month = " + (datePicker.getMonth()+1);
        String year = "Year = " + datePicker.getYear();

        Toast.makeText(getApplicationContext(), day + "\n" + month + "\n" + year, Toast.LENGTH_LONG).show();

        location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent findLocationIntent = new Intent(MainActivity.this, TrackerLocationActivity.class);
                startActivity(findLocationIntent);
            }
        });
    }
}
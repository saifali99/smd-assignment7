package com.example.assignment7;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

public class LoginActivtiy extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        getSupportActionBar().hide();

        setContentView(R.layout.activity_login_activtiy);

        EditText username = (EditText) findViewById(R.id.et_username);
        EditText password = (EditText) findViewById(R.id.et_password);
        CheckBox remeberMe = (CheckBox) findViewById(R.id.cb_rememberMe);
        Button login = (Button) findViewById(R.id.btn_login);

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0);
        SharedPreferences.Editor editor = pref.edit();

        if(pref.getString("username", null) != null) {
            Intent intent = new Intent(LoginActivtiy.this, MainActivity.class);
            startActivity(intent);
        }

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(remeberMe.isChecked()) {
                    editor.putString("username", username.getText().toString());
                    editor.commit();
                }
                if(username.getText().toString().equals("student") && password.getText().toString().equals("student")) {
                        Intent intent = new Intent(LoginActivtiy.this, MainActivity.class);
                        startActivity(intent);
                }
            }
        });
    }
}
package com.example.assignment7;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class TrackerLocationActivity extends AppCompatActivity {

    private GpsTracker gpsTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tracker_location);

        TextView latitude = (TextView) findViewById(R.id.tv_latitude);
        TextView longitude = (TextView) findViewById(R.id.tv_longitude);

        gpsTracker = new GpsTracker(TrackerLocationActivity.this);
        if(gpsTracker.canGetLocation()) {
            double lat = gpsTracker.getLatitude();
            double lon = gpsTracker.getLongitude();

            latitude.setText("Latitude: " + lat);
            longitude.setText("Longitude: " + lon);

            System.out.println(lat + " - " + lon);
        }else {
            gpsTracker.showSettingsAlert();
        }
    }
}